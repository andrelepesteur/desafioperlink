import { Component, OnInit } from '@angular/core';
import { ProcessosService } from '../../app/Services/processo.service';
import { IProcesso } from '../Models/processo.interface';
@Component({
  selector: 'app-processo',
  templateUrl: './processo.component.html'
})
export class ProcessoComponent implements OnInit {

  processo: IProcesso[] = [];

  constructor(private processoService: ProcessosService) { }

  private getProcessos() {
    this.processoService.getProcessos().subscribe(
      data => this.processo = data,
      error => alert(error),
      () => console.log(this.processo)
    );
  }
  ngOnInit() {

    this.getProcessos();
  }

}
