import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { IProcesso } from '../Models/processo.interface';

@Injectable()
export class ProcessosService {

  constructor(private http: Http) { }

  //get

  getProcessos() {
    return this.http.get("/api/Processo").map(data => <IProcesso[]>data.json());
  }

}
