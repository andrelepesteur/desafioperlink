﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using App.Model;

namespace App.Controllers
{
    [Route("api/Processo")]
    [ApiController]
    public class ProcessoController : ControllerBase
    {
        private readonly masterContext _context;

        public ProcessoController(masterContext context)
        {
            _context = context;
        }

        // GET: api/Processoes
        [HttpGet]
        public IEnumerable<Processo> GetProcesso()
        {
            return _context.Processo;
        }

        // GET: api/Processoes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProcesso([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var processo = await _context.Processo.FindAsync(id);

            if (processo == null)
            {
                return NotFound();
            }

            return Ok(processo);
        }

        // PUT: api/Processoes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProcesso([FromRoute] int id, [FromBody] Processo processo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != processo.IdProcessos)
            {
                return BadRequest();
            }

            _context.Entry(processo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProcessoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Processoes
        [HttpPost]
        public async Task<IActionResult> PostProcesso([FromBody] Processo processo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Processo.Add(processo);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProcessoExists(processo.IdProcessos))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetProcesso", new { id = processo.IdProcessos }, processo);
        }

        // DELETE: api/Processoes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProcesso([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var processo = await _context.Processo.FindAsync(id);
            if (processo == null)
            {
                return NotFound();
            }

            _context.Processo.Remove(processo);
            await _context.SaveChangesAsync();

            return Ok(processo);
        }

        private bool ProcessoExists(int id)
        {
            return _context.Processo.Any(e => e.IdProcessos == id);
        }
    }
}