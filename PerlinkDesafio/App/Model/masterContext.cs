﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace App.Model
{
    public partial class masterContext : DbContext
    {
        public masterContext()
        {
        }

        public masterContext(DbContextOptions<masterContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Processo> Processo { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Data Source=DESKTOP-O34FIBM\\SQLEXPRESS01;Initial Catalog=master;Integrated Security=True");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Processo>(entity =>
            {
                entity.HasKey(e => e.IdProcessos);

                entity.Property(e => e.IdProcessos)
                    .HasColumnName("id_processos")
                    .ValueGeneratedNever();

                entity.Property(e => e.DataDeCriacaoDoProcesso)
                    .HasColumnName("dataDeCriacaoDoProcesso")
                    .HasColumnType("datetime");

                entity.Property(e => e.Escritorio)
                    .HasColumnName("escritorio")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumeroDoProcesso)
                    .HasColumnName("numeroDoProcesso")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Valor)
                    .HasColumnName("valor")
                    .HasColumnType("decimal(18, 0)");
            });
        }
    }
}
