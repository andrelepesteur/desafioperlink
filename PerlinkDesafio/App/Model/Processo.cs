﻿using System;
using System.Collections.Generic;

namespace App.Model
{
    public partial class Processo
    {
        public int IdProcessos { get; set; }
        public string NumeroDoProcesso { get; set; }
        public DateTime? DataDeCriacaoDoProcesso { get; set; }
        public decimal? Valor { get; set; }
        public string Escritorio { get; set; }
    }
}
